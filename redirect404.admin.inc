<?php

/**
 * @file
 * Administrative page callbacks for the redirect404 module.
 */

/**
 * redirect404 administration settings.
 */
function redirect404_settings() {
  $form['redirect404_allow_external_url'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow external URLs.'),
    '#default_value' => variable_get('redirect404_allow_external_url', 1),
    '#description' => t('When enabled also redirects to external URLs (i.e. http://example.com) are allowed.'),
  );

  return system_settings_form($form);
}

/**
 * Redirect 404 edit form.
 *
 * Almost the same as path_redirect_edit_form, only to get an own form id,
 * to add more validation.
 */
function redirect404_path_redirect_edit_form(&$form_state, $redirect = array()) {
  // Show Redirect URL form with query as source
  $redirect += array(
    'source' => $_REQUEST['q'],
    'redirect404' => TRUE,
  );

  module_load_include('inc', 'path_redirect', 'path_redirect.admin');
  $form = path_redirect_edit_form($form_state, $redirect);

  // Hide the source field - is already defined above
  $form['source']['#type'] = 'value';

  // Show source in a message field
  $form['redirect404_message'] = array(
    '#value' => t('You may enter a redirect path or URI for the non existing path <strong>!source</strong> below.', array('!source' => url($_REQUEST['q'], array('absolute' => TRUE)) . (variable_get('clean_url', 0) ? '' : '?q='))),
    '#prefix' => '<div class="messages status">',
    '#suffix' => '</div>',
    '#weight' => -50,
  );

  // Set Cancel link to Javascript history - 1
  $form['cancel']['#value'] = sprintf('<a href="javascript: history.go(-1)">%s</a>', t('Cancel'));

  // Insert language chooser if necessary.
  // See locale_form_path_redirect_edit_form_alter in path_redirect.admin.inc.
  if (function_exists('locale_language_list')) {
    $form['language'] = array(
      '#type' => 'select',
      '#title' => t('Language'),
      '#options' => array('' => t('All languages')) + locale_language_list('name'),
      '#default_value' => $form['language']['#value'],
      '#weight' => 0,
      '#description' => t('A redirect set for a specific language will always be used when requesting this page in that language, and takes precedence over redirects set for <em>All languages</em>.'),
    );
  }

  return $form;
}

/**
 * Implements form validation.
 */
function redirect404_path_redirect_edit_form_validate($form, &$form_state) {

  path_redirect_edit_form_validate($form, $form_state);

  // In case the user entered an internal path as absolute URL we 
  // just remove schema, host etc. from it. 
  // I.e. http://yourdrupaldomain.com/path => path

  // Get the schema and host URL parts of the current host
  $path_prefix = url('', array('absolute' => TRUE)) . (variable_get('clean_url', 0) ? '' : '?q=');  

  // Strip leading path_prefix
  $form_state['values']['redirect'] = preg_replace('%^' . preg_quote($path_prefix) . '%', '', $form_state['values']['redirect']);

  // Strip leading slashes
  $form_state['values']['redirect'] = ltrim($form_state['values']['redirect'], '/');

  // Nothing more to do, this is default behavior of path_redirect
  if (variable_get('redirect404_allow_external_url', 1)) {
    return;
  }

  // If external URLs are not allowed: some more limitations to validate.
  $url = parse_url($form_state['values']['redirect']);
  if ($url['scheme'] || $url['host'] || $url['port'] || $url['user'] || $url['pass']) {
    form_set_error('redirect', t('Only internal URLs are allowed.'));
  }
}

/**
 * Implements form submission.
 */
function redirect404_path_redirect_edit_form_submit($form, &$form_state) {
  path_redirect_edit_form_submit($form, $form_state);
}

/**
 * Autocompletion callback for the add/edit redirect form. Returns a list of
 * current paths on the site.
 */
function redirect404_js_autocomplete() {
  $args = func_get_args();
  $string = drupal_strtolower(implode('/', $args));
  $matches = array();

  // Get a list of 404s, sorted by the number of times each 404 was processed.
  $paths = db_query("SELECT dst FROM {url_alias} WHERE LOWER(dst) LIKE '%%%s%%' ORDER BY dst ASC", drupal_strtolower($string));
  $i = 0;
  while (($path = db_result($paths)) && $i < 20) {
    // If already has a redirect, discard it.
    if (!path_redirect_load_by_source($path)) {
      $matches[$path] = check_plain($path);
      $i++;
    }
  }

  // Return result as JSON.
  drupal_json($matches);
}